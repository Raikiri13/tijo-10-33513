package pl.edu.pwsztar.domain.dto;

import java.util.Arrays;

public class Point2D {

    private final String[] chessColumns = {"a","b","c","d","e","f","g","h"};
    public int X;
    public int Y;

    Point2D(String stringCoordinates){
        final String[] coordinates = stringCoordinates.split("_"); // under index 0 is alphabetic coordinate and under index 1 is numeric coordinate
        this.X = Integer.parseInt(coordinates[1]);
        this.Y = Arrays.asList(chessColumns).indexOf(coordinates[0]);
    }

    static public Point2D convertChessCoordinatesPoint2D(String stringCoordinates){
        return new Point2D(stringCoordinates);
    }

    @Override
    public String toString() {
        return "Point2D{" +
                "X=" + X +
                ", Y=" + Y +
                '}';
    }
}
