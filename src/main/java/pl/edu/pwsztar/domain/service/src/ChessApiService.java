package pl.edu.pwsztar.domain.service.src;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.domain.dto.Point2D;
import pl.edu.pwsztar.domain.enums.FigureType;

import java.util.Arrays;

public class ChessApiService implements pl.edu.pwsztar.domain.service.ChessApiService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ChessApiService.class);

    @Override
    public Boolean checkMove(FigureMoveDto figureMoveDto){

        final String[] chessColumns = {"a","b","c","d","e","f","g","h"};
        Point2D sourceCoordinates = Point2D.convertChessCoordinatesPoint2D(figureMoveDto.getSource());
        Point2D desCoordinates = Point2D.convertChessCoordinatesPoint2D(figureMoveDto.getDestination());
        LOGGER.info("sourcecoords in service: {}",sourceCoordinates);

        if( figureMoveDto.getType() == FigureType.BISHOP) {
            int moveRowDistance = desCoordinates.X - sourceCoordinates.X;
            LOGGER.info("moverowdistance: {}",moveRowDistance);
            if ( sourceCoordinates.Y + moveRowDistance == desCoordinates.Y ||
                    sourceCoordinates.Y - moveRowDistance == desCoordinates.Y
                ){
                LOGGER.info("*** result of service checkMove : {}", true);

                return true;
            } else {
                LOGGER.info("*** result of service checkMove : {}", false);
                return false;
            }
        }
        return false;
    }



}
